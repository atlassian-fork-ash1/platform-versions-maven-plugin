package com.atlassian.platform.maven;

import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class MavenXPath {
    // Maven XML uses namespace, needs to be known for dom4j Xpath engine to work
    private static final Map<String, String> NAMESPACES = Collections.singletonMap("xmlns", "http://maven.apache.org/POM/4.0.0");

    private final Node doc;

    MavenXPath(InputStream in) throws DocumentException {
        this.doc = new SAXReader().read(in);
    }

    MavenXPath(Node doc) {
        this.doc = doc;
    }

    public List<Node> selectNodes(String xpath) {
        XPath p = doc.createXPath(xpath);
        p.setNamespaceURIs(NAMESPACES);
        return p.selectNodes(doc);
    }

    public String valueOf(String xpath) {
        XPath p = doc.createXPath(xpath);
        p.setNamespaceURIs(NAMESPACES);
        return p.valueOf(doc);
    }
}
