package com.atlassian.platform.maven;

import org.apache.commons.io.input.BoundedInputStream;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.repository.RepositorySystem;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Mojo(name = "generate")
public class GenerateVersionsFileMojo extends AbstractMojo {

    /*
     * What version of platform-poms use for checks generation
     */
    @Parameter(defaultValue = "${platform.version}", required = true)
    private String platformVersion;

    /*
     * What groupId of platform-poms use for checks generation
     */
    @Parameter(defaultValue = "com.atlassian.platform", required = true)
    private String platformGroupId;

    /*
     * Local maven repository, used to get platform dependencies for analysis
     */
    @Parameter(defaultValue = "${localRepository}", readonly = true, required = true)
    private ArtifactRepository localRepository;

    /*
     * Remote repositories configured for the current project, used to fetch platform 
     * dependencies when it missed in local maven cache
     */
    @Parameter(defaultValue = "${project.remoteArtifactRepositories}", readonly = true, required = true)
    private List<ArtifactRepository> remoteRepositories;

    /*
     * Path where plugin should create final checks file
     */
    @Parameter(defaultValue = "src/main/resources/platformversions.xml", required = true)
    private File output;

    /*
     * List of platform dependencies to be excluded from analysis. Should be provided in form
     * of group:artifact
     */
    @Parameter(required = false)
    private Set<String> excludes = new HashSet<>();

    /*
     * Maven subsystem for repository related work: artifact creation, dependencies management, e.t.c
     */
    @Component
    private RepositorySystem repositorySystem;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        final Artifact platformToFind = repositorySystem.createProjectArtifact(
                platformGroupId,
                "platform",
                platformVersion
        );
        final Artifact platform = resolve(platformToFind)
                .orElseThrow(() -> new RuntimeException("Failed to find " + platformToFind));

        // Create output document
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("platform");
        root.addAttribute("version", platformVersion);

        // JAR file ZIP stream scaned in a single pass fashion, so it is not possible
        // to say will atlassian-xml be before manifest or after. Also it could be that
        // some JARs are OSGi bundles but NOT atlassian plugins. Because of that generation
        // will be performed in 2 passed:
        // 1. fill tmp document
        // 2. validate result
        // 3. move data from tmp tag into root tag
        //
        // That is the way to trade memory for IO
        Element tmp = root.addElement("tmp");
        PlatformChecks platformChecks = new PlatformChecks(tmp);

        // Add all dependencies
        List<Artifact> platformDeps = new PlatformPom(repositorySystem, platform.getFile()).dependencies();
        for (Artifact a : platformDeps) {
            Optional<Artifact> maybeDep = resolve(a);
            if (!maybeDep.isPresent()) {
                getLog().warn("Failed to resolve artifact: " + a);
                root.addComment(a.toString() + ": failed to resolve");
                continue;
            }

            final Artifact dep = maybeDep.get();

            if (excludes.contains(a.getGroupId() + ":" + a.getArtifactId())) {
                getLog().warn("Skip excluded artifact: " + a);
                root.addComment(dep.toString() + ": skiped by exclusion");
                continue;
            }

            // Single pass sequence IO over ZIP archive to find and parse pplugin & Bundle information
            boolean isAtlassianPlugin = false;
            try (ZipInputStream in = new ZipInputStream(new FileInputStream(dep.getFile()))) {
                ZipEntry e = null;
                while ((e = in.getNextEntry()) != null) {
                    // skip directory
                    if (e.isDirectory()) {
                        continue;
                    }

                    // Setup stream for a single entry
                    BoundedInputStream entryStream = new BoundedInputStream(in, e.getSize());
                    entryStream.setPropagateClose(false);

                    // generate checks
                    if ("atlassian-plugin.xml".equals(e.getName())) {
                        platformChecks.addPlugin(entryStream);
                        isAtlassianPlugin = true;
                    } else if ("META-INF/MANIFEST.MF".equals(e.getName())) {
                        platformChecks.addService(entryStream);
                    }

                    // Close current entry
                    in.closeEntry();
                }
            } catch (IOException e) {
                throw new MojoExecutionException("Failed to read dependency archive", e);
            } catch (Exception e) {
                throw new MojoExecutionException("Failed to generate dependency check", e);
            }

            // Validate result and move data from tmp tag into root in case if
            // atlassian-plugin.xml was found. For simple bundles nothing generated
            root.addComment(dep.toString());
            if (isAtlassianPlugin) {
                // get bundle name and update service checks
                String name = tmp.valueOf("plugin-version-check/@modulename");
                for (Element check : (List<Element>) tmp.selectNodes("export-version-check")) {
                    check.addAttribute("modulename", name);
                }

                // move results to final root
                for (Element e : (List<Element>) tmp.elements()) {
                    e.detach();
                    root.add(e);

                    // TODO: remove that once all filter will be in place
                    e.addAttribute("optional", "true");
                }
            }

            // cleat temp in case if it was a bundle
            for (Element e : (List<Element>) tmp.elements()) {
                tmp.remove(e);
            }
        }

        // Output result, delete tmp tag before
        tmp.detach();
        OutputFormat format = OutputFormat.createPrettyPrint();
        try {
            XMLWriter writer = new XMLWriter(new FileWriter(output), format);
            writer.write(document);
            writer.close();
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to write final xml", e);
        }
    }

    private Optional<Artifact> resolve(Artifact a) {
        ArtifactResolutionRequest request = new ArtifactResolutionRequest();
        request.setArtifact(a);
        request.setLocalRepository(localRepository);
        request.setRemoteRepositories(remoteRepositories);
        request.setResolveTransitively(false);
        getLog().debug("Request to resolve: " + request);

        ArtifactResolutionResult result = repositorySystem.resolve(request);
        if (result.hasExceptions()) {
            throw new RuntimeException("Failed to resolve " + a, result.getExceptions().get(0));
        }

        for (Artifact c : result.getArtifacts()) {
            if (a.getGroupId().equals(c.getGroupId()) && a.getArtifactId().equals(c.getArtifactId())) {
                return Optional.of(c);
            }
        }

        return Optional.empty();
    }
}